package com.aliceapp.texasholdem.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PlayAction {
    FOLD, // gives up
    CHECK, // continues
    RAISE,  // increases the value
    CALL; // accepts the increased value

    public static PlayAction[] ACTIONS_CHECK = {FOLD, CHECK, RAISE};
    public static PlayAction[] ACTIONS_CALL = {FOLD, RAISE, CALL};
}
