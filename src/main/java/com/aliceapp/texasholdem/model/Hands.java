package com.aliceapp.texasholdem.model;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Hands {

    private int small = 0;
    private int big = 1;
    private int dealer = 2;

    @Setter
    private int playing = 2;

    public void increment() {
        big++;
        small++;
        dealer++;
    }
}
