package com.aliceapp.texasholdem.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lombok.Data;

@Data
public class Board {

    private List<Card> cards;
    private final List<Card> revealed = new ArrayList<>(5);

    public void setCards(List<Card> cards) {
        this.cards = new ArrayList<>(cards);
        this.revealed.clear();
    }

    public void play() {
        if (cards.isEmpty()) {
            return;
        }

        Iterator<Card> bucket = cards.iterator();

        // on first time reveals 2 cards
        if (revealed.isEmpty()) {
            revealed.add(bucket.next());
            bucket.remove();
        }

        revealed.add(bucket.next());
        bucket.remove();
    }
}
