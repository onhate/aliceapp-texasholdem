package com.aliceapp.texasholdem.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;

@Data
public class BalanceAndBet {

    public static final int HALF_BET = 50;
    public static final int FULL_BET = 100;

    private int balance;
    private int currentBet = FULL_BET;
    private Map<Player, Integer> payments;

    public BalanceAndBet() {
        reset();
    }

    public void endRound(List<Player> winners) {
        int amount = (balance / winners.size());
        winners.forEach(winner -> winner.addBalance(amount));
        reset();
    }

    public void paySmallBlind(Player player) {
        balance += pay(player, BalanceAndBet.HALF_BET);
    }

    public void payBigBlind(Player player) {
        balance += pay(player, BalanceAndBet.FULL_BET);
    }

    public void bet(Player player, Integer amount) {
        currentBet += amount;
        payBet(player);
    }

    public void payBet(Player player) {
        balance += pay(player, currentBet);
    }

    public Integer getAlreadyPaid(Player player) {
        return payments.computeIfAbsent(player, p1 -> 0);
    }

    public Integer getPendingPayment(Player player) {
        Integer paid = getAlreadyPaid(player);
        return Math.max(currentBet - paid, 0);
    }

    private int pay(Player player, int value) {
        payments.putIfAbsent(player, 0);

        Integer paid = payments.get(player);
        if (paid < value) {
            int amount = player.pay(value - paid);
            payments.merge(player, amount, Integer::sum);
            return amount;
        }
        return 0;
    }

    private void reset() {
        balance = 0;
        currentBet = FULL_BET;
        payments = new HashMap<>();
    }
}
