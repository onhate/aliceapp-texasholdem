package com.aliceapp.texasholdem.model;

import com.aliceapp.texasholdem.game.Game;
import com.aliceapp.texasholdem.rules.Evaluator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;

public class BotPlayer extends Player {

    private static final int DEFAULT_BET = 100;
    private static final int CALL_THRESHOLD = 10;
    private static final int RAISE_THRESHOLD = 20;

    private int raises = 0;

    private final Evaluator evaluator = new Evaluator();

    public BotPlayer(String name) {
        super(name);
    }

    @Override
    public PlayAction play(Game game, PlayAction[] available) {
        thinkAboutLife();

        if (ArrayUtils.contains(available, PlayAction.RAISE)) {
            int chances = getCurrentChances(game);
            if (chances >= RAISE_THRESHOLD && (raises++ % 2 == 0)) {
                return PlayAction.RAISE;
            }
        }

        if (ArrayUtils.contains(available, PlayAction.CALL)) {
            // not enough cards to decide
            int revealed = game.getBoardRevealed().size();
            if (revealed < 2) {
                return PlayAction.CALL;
            }

            // calculate chances
            int chances = getCurrentChances(game);
            if (chances >= CALL_THRESHOLD) {
                return PlayAction.CALL;
            }
        }

        if (ArrayUtils.contains(available, PlayAction.CHECK)) {
            return PlayAction.CHECK;
        }
        return PlayAction.FOLD;
    }

    @Override
    public Integer getBet() {
        return DEFAULT_BET;
    }

    private int getCurrentChances(Game game) {
        List<Card> all = ListUtils.union(game.getBoardRevealed(), cards);
        return evaluator.evaluate(all).stream().findFirst()
                .map(result -> result.getPriority() * 10)
                .orElse(0);
    }

    private void thinkAboutLife() {
        try {
            int seconds = RandomUtils.nextInt(1, 3);
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException ignored) {
        }
    }
}
