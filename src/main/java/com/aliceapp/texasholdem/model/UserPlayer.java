package com.aliceapp.texasholdem.model;

import static java.lang.Integer.parseInt;
import static java.util.stream.IntStream.rangeClosed;
import static org.apache.commons.lang3.StringUtils.join;

import com.aliceapp.texasholdem.game.Game;
import com.aliceapp.texasholdem.utils.IO;
import java.util.stream.Stream;
import org.apache.commons.lang3.text.WordUtils;

public class UserPlayer extends Player {

    public UserPlayer(String name) {
        super(name);
    }

    @Override
    public PlayAction play(Game game, PlayAction[] available) {
        String[] commands = rangeClosed(1, available.length)
                .mapToObj(String::valueOf)
                .toArray(String[]::new);

        String message = join(Stream.of(available)
                .map(Enum::name)
                .map(WordUtils::capitalizeFully)
                .toArray(String[]::new), ", ");

        String command = IO.read(message, commands);
        int ordinal = parseInt(command) - 1;
        return available[ordinal];
    }

    @Override
    public Integer getBet() {
        String amount = IO.read("How much?", "100", "200", "300");
        return parseInt(amount);
    }
}
