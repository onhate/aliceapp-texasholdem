package com.aliceapp.texasholdem.model;

import static org.apache.commons.lang3.StringUtils.join;

import com.aliceapp.texasholdem.game.Game;
import com.aliceapp.texasholdem.rules.RuleResult;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "name", callSuper = false)
public abstract class Player {

    private static final int INITIAL_BALANCE = 10000;

    private final String name;

    private Integer balance = INITIAL_BALANCE;
    private List<RuleResult> results;
    private PlayAction previousAction;
    protected List<Card> cards;

    public abstract PlayAction play(Game game, PlayAction[] available);

    public abstract Integer getBet();

    public void resetPreviousAction() {
        this.previousAction = null;
    }

    int pay(int amount) {
        // no more founds
        if (balance == 0) {
            return 0;
        }

        // sufficient found
        if (amount <= balance) {
            balance -= amount;
            return amount;
        }

        // insufficient found
        int diff = amount - balance;
        balance = 0;
        return diff;
    }

    void addBalance(int amount) {
        balance += amount;
    }

    public boolean isPlaying() {
        return previousAction != PlayAction.FOLD;
    }


    @Override
    public String toString() {
        return String.format("%s (%d) %s", name, balance, join(cards, " "));
    }
}
