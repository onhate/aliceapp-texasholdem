package com.aliceapp.texasholdem.model;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringEscapeUtils.unescapeJava;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Card implements Comparable<Card> {

    private static final String SUITE_ASCII_BASE[] = {"a", "b", "c", "d"};

    private final int value;
    private final Suites suite;

    /**
     * https://en.wikipedia.org/wiki/Playing_cards_in_Unicode
     */
    private String getAsciiValue() {
        String base = SUITE_ASCII_BASE[suite.ordinal()];
        String end = Integer.toHexString((value > 11) ? value + 1 : value);
        return unescapeJava(format("\\ud83c\\udc%s%s", base, end));
    }

    @Override
    public String toString() {
        return getAsciiValue();
    }

    @Override
    public int compareTo(Card other) {
        if (value > other.getValue()) {
            return 1;
        }
        if (value < other.getValue()) {
            return -1;
        }
        return suite.compareTo(other.getSuite());
    }
}
