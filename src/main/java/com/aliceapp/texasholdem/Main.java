package com.aliceapp.texasholdem;

import static java.lang.Integer.parseInt;
import static java.util.stream.IntStream.rangeClosed;

import com.aliceapp.texasholdem.game.Game;
import com.aliceapp.texasholdem.model.Player;
import com.aliceapp.texasholdem.utils.IO;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        IO.clearScreen();
        IO.out("Welcome to Texas Hold'em Poker!\n");

        String action = IO.read("do you wanna play or watch the bots to play?", "P", "W");
        int users = "P".equals(action) ? 1 : 0; // playing 1-8, watching 2-9
        String[] qty = rangeClosed(2 - users, 9 - users).mapToObj(Integer::toString).toArray(String[]::new);
        int bots = parseInt(IO.read("how many bots do you want to add to the game?", qty));

        Game game = new Game(bots, users);
        do {
            game.distributeCards();
            game.initialize();
            IO.printBoard(game);

            game.play(IO::printBoard);

            List<Player> winners = game.evaluate();
            IO.printBoard(game, true);
            IO.printWinners(winners);
        } while (IO.next());
    }
}
