package com.aliceapp.texasholdem.game;

import static java.util.Arrays.asList;

import com.aliceapp.texasholdem.model.BalanceAndBet;
import com.aliceapp.texasholdem.model.Board;
import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Hands;
import com.aliceapp.texasholdem.model.PlayAction;
import com.aliceapp.texasholdem.model.Player;
import com.aliceapp.texasholdem.rules.Evaluator;
import com.aliceapp.texasholdem.rules.RuleResult;
import com.aliceapp.texasholdem.rules.Rules;
import com.aliceapp.texasholdem.utils.CircularList;
import com.aliceapp.texasholdem.utils.IO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import lombok.Getter;
import org.apache.commons.collections4.ListUtils;

@Getter
public class Game {

    private final Evaluator evaluator = new Evaluator();
    private final Board board = new Board();
    private final Hands hands = new Hands();
    private final BalanceAndBet balances = new BalanceAndBet();

    private final CircularList<Player> players;

    Game(CircularList<Player> players) {
        this.players = players;
    }

    public Game(int bots, int users) {
        this(Builder.players(bots, users));
    }

    public void distributeCards() {
        Iterator<Card> dist = Builder.cards().iterator();
        // each player receives 2 cards
        players.forEach(player -> player.setCards(asList(dist.next(), dist.next())));
        // board receives 5 cards
        board.setCards(asList(dist.next(), dist.next(), dist.next(), dist.next(), dist.next()));
    }

    public void initialize() {
        hands.increment();
        // make required payments
        Player small = players.get(hands.getSmall());
        Player big = players.get(hands.getBig());

        balances.paySmallBlind(small);
        IO.log(this, "=> %s paid the small blind\n", small.getName());
        balances.payBigBlind(big);
        IO.log(this, "=> %s paid the big blind\n", big.getName());

        // reset previous round action, fresh start
        players.forEach(Player::resetPreviousAction);
    }

    public void play(Consumer<Game> iterator) {
        iterate();
        iterator.accept(this);
        iterate();
        iterator.accept(this);
        iterate();
        iterator.accept(this);
        iterate();
        iterator.accept(this);
        iterate();
        iterator.accept(this);
    }

    void iterate() {
        // still active players
        long active = players.stream()
                .filter(Player::isPlaying)
                .count();
        if (active == 1) {
            return;
        }

        int hand = hands.getDealer();
        iterate(hand);
        board.play();
    }

    private void iterate(int start) {
        int size = players.size();
        for (int i = start; i < (start + size); i++) {
            hands.setPlaying(i);
            Player player = players.get(i);

            // if skipped on previous iterations, by pass
            if (!player.isPlaying()) {
                continue;
            }

            Integer pending = balances.getPendingPayment(player);
            PlayAction[] actions = pending > 0 ? PlayAction.ACTIONS_CALL : PlayAction.ACTIONS_CHECK;
            if (pending > 0) {
                IO.logAndWait(this, "=> %s must pay %d\n", player.getName(), pending);
            } else {
                IO.logAndWait(this, "=> %s turn\n", player.getName());
            }

            PlayAction action = player.play(this, actions);
            player.setPreviousAction(action);

            if (action == PlayAction.CHECK) {
                IO.logAndWait(this, "=> %s checked\n", player.getName());
                continue;
            }

            if (action == PlayAction.FOLD) {
                IO.logAndWait(this, "=> %s folded\n", player.getName());
                continue;
            }

            if (action == PlayAction.CALL) {
                balances.payBet(player);
                IO.logAndWait(this, "=> %s paid the bet\n", player.getName());
            }

            if (action == PlayAction.RAISE) {
                Integer bet = player.getBet();
                balances.bet(player, bet);
                IO.logAndWait(this, "=> %s raised %d the bet\n", player.getName(), bet);
                iterate(i + 1); // start a new iteration from the next
                break; // stop current loop
            }
        }
    }

    public List<Player> evaluate() {
        List<Player> winners = new ArrayList<>();

        List<RuleResult> max = null;
        for (Player player : players) {
            if (!player.isPlaying()) {
                continue;
            }

            List<Card> cards = getAllCards(player);
            List<RuleResult> results = evaluator.evaluate(cards);
            player.setResults(results);

            int compare = Rules.compare(results, max);
            if (compare > 0) { // bigger hand, new winner
                winners = new ArrayList<>();
                winners.add(player);
                max = results;
            }
            if (compare == 0) { // same hand, both wins
                winners.add(player);
            }
        }
        balances.endRound(winners);
        return winners;
    }

    public List<Card> getBoardCards() {
        return board.getCards();
    }

    public List<Card> getBoardRevealed() {
        return board.getRevealed();
    }

    public boolean isDealer(Player player) {
        return player == players.get(hands.getDealer());
    }

    public boolean isPlaying(Player player) {
        return player == players.get(hands.getPlaying());
    }

    public boolean isSmall(Player player) {
        return player == players.get(hands.getSmall());
    }

    public boolean isBig(Player player) {
        return player == players.get(hands.getBig());
    }

    private List<Card> getAllCards(Player player) {
        List<Card> revealed = board.getRevealed();
        List<Card> cards = player.getCards();
        return ListUtils.union(cards, revealed);
    }

    // visible for testing

    Player getPlayer(int index) {
        return players.get(index);
    }
}
