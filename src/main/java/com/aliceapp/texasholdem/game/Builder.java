package com.aliceapp.texasholdem.game;

import com.aliceapp.texasholdem.model.BotPlayer;
import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Player;
import com.aliceapp.texasholdem.model.Suites;
import com.aliceapp.texasholdem.model.UserPlayer;
import com.aliceapp.texasholdem.utils.CircularList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Builder {

    static List<Card> cards() {
        List<Card> cards = new ArrayList<>();
        for (int i = 1; i <= 13; i++) {
            cards.add(new Card(i, Suites.SPADES));
            cards.add(new Card(i, Suites.HEARTS));
            cards.add(new Card(i, Suites.CLUBS));
            cards.add(new Card(i, Suites.DIAMONDS));
        }
        Collections.shuffle(cards);
        return cards;
    }

    static CircularList<Player> players(int bots, int users) {
        CircularList<Player> players = new CircularList<>();
        for (int i = 0; i < users; i++) {
            Player player = new UserPlayer(String.format("User %d", i + 1));
            players.add(player);
        }
        for (int i = 0; i < bots; i++) {
            Player player = new BotPlayer(String.format("Bot %d", i + 1));
            players.add(player);
        }
        return players;
    }
}
