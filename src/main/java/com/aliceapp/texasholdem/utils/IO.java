package com.aliceapp.texasholdem.utils;

import static java.lang.String.format;
import static org.apache.commons.lang3.ArrayUtils.contains;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.rightPad;
import static org.apache.commons.lang3.StringUtils.upperCase;

import com.aliceapp.texasholdem.game.Game;
import com.aliceapp.texasholdem.model.BalanceAndBet;
import com.aliceapp.texasholdem.model.Player;
import com.aliceapp.texasholdem.model.UserPlayer;
import com.aliceapp.texasholdem.rules.RuleResult;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import lombok.experimental.UtilityClass;

@UtilityClass
public class IO {

    private static final Scanner scanner = new Scanner(System.in);

    public static boolean next() {
        String option = read("continue?", "Y", "N");
        return !"N".equals(option);
    }

    public static String read(String message, String... options) {
        while (true) {
            out("%s [%s]: ", message, join(options, "/"));
            String next = upperCase(scanner.next());
            if (contains(options, next)) {
                return next;
            }
        }
    }

    public static void log(Game game, String value, Object... args) {
        printBoard(game);
        out(value, args);
    }

    public static void logAndWait(Game game, String value, Object... args) {
        printBoard(game);
        out(value, args);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException ignored) {
        }
    }

    public static void out(String value, Object... args) {
        System.out.print(format(value, args));
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
    }

    public static void printBoard(Game game) {
        printBoard(game, false);
    }

    public static void printBoard(Game game, boolean showCards) {
        clearScreen();
        out("          \uD83C\uDCAB Texas Hold'em \uD83C\uDCBB          \n");

        BalanceAndBet balance = game.getBalances();
        out("%s :%d\n", rightPad("Current Balance", 20, "."), balance.getBalance());
        out("%s :%d\n", rightPad("Bet", 20, "."), balance.getCurrentBet());

        out("%s :", rightPad("Board Cards", 20));
        game.getBoardRevealed().forEach(card -> out("%s  ", card));
        game.getBoardCards().forEach(card -> out("\uD83C\uDCA0  ")); // black card
        out("\n");

        printPlayers(game, showCards);
    }

    public static void printWinners(List<Player> winners) {
        out("------------------------------------------------------------------\n");
        winners.forEach(winner -> {
            out("%s won with the following hand\n", winner.getName());
            for (RuleResult result : winner.getResults()) {
                String hand = rightPad(result.getName(), 20, ".");
                String cards = join(result.getCards(), "  ");
                out(" %s -> %s \n", hand, cards);
            }
        });
        out("\n");
    }

    private static void printPlayers(Game game, boolean showCards) {
        out("------------------------------------------------------------------\n");
        game.getPlayers().forEach(player -> {
            String name = rightPad(player.getName(), 20, ".");
            Integer balance = player.getBalance();
            Integer paid = game.getBalances().getAlreadyPaid(player);

            String playing = " ";
            if (!player.isPlaying()) {
                playing = "X";
            } else if (game.isPlaying(player)) {
                playing = ">";
            }

            String prefix = " ";
            if (game.isSmall(player)) {
                prefix = "S";
            }
            if (game.isBig(player)) {
                prefix = "B";
            }
            if (game.isDealer(player)) {
                prefix = "D";
            }

            if (showCards || player instanceof UserPlayer) {
                String cards = join(player.getCards(), "  ");
                out("%s[%s] %s: (%d/%d) -> %s \n", playing, prefix, name, paid, balance, cards);
            } else {
                out("%s[%s] %s: (%d/%d)\n", playing, prefix, name, paid, balance);
            }
        });
    }
}
