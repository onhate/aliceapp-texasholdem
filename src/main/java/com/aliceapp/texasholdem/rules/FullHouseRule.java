package com.aliceapp.texasholdem.rules;

import static java.util.Collections.emptyList;
import static java.util.Comparator.reverseOrder;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.ListUtils;

public class FullHouseRule implements Rule {

    @Override
    public int priority() {
        return 6;
    }

    @Override
    public String name() {
        return "Full House";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < 5) {
            return RuleResult.NO_MATCH;
        }

        Map<Integer, List<Card>> grouped = Rules.groupBy(cards, Card::getValue, reverseOrder());

        // find triples
        List<Card> triple = Rules.filterBySize(grouped, 3).findFirst().orElse(emptyList());
        if (triple.isEmpty()) {
            return RuleResult.NO_MATCH;
        }

        // remove the cards containing value of the 3s
        int threesValue = triple.get(0).getValue();
        grouped.remove(threesValue);

        // find pairs excluding triples
        List<Card> pairs = Rules.filterBySize(grouped, 2).findFirst().orElse(emptyList());
        if (pairs.isEmpty()) {
            return RuleResult.NO_MATCH;
        }

        return RuleResult.matching(ListUtils.union(triple, pairs));
    }
}
