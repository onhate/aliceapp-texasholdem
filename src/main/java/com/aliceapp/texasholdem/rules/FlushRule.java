package com.aliceapp.texasholdem.rules;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.List;
import java.util.Map;

public class FlushRule implements Rule {

    @Override
    public int priority() {
        return 5;
    }

    @Override
    public String name() {
        return "Flush";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < 5) {
            return RuleResult.NO_MATCH;
        }

        Map<Suites, List<Card>> grouped = Rules.groupBy(cards, Card::getSuite);
        return Rules.filterBySizeSortingByValues(grouped, 5).findFirst()
                .map(RuleResult::matching)
                .orElse(RuleResult.NO_MATCH);
    }
}
