package com.aliceapp.texasholdem.rules;

import static java.util.Comparator.reverseOrder;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;
import java.util.Map;

public class ThreeOfAKindRule implements Rule {

    @Override
    public int priority() {
        return 3;
    }

    @Override
    public String name() {
        return "Three of a Kind";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < 3) {
            return RuleResult.NO_MATCH;
        }

        Map<Integer, List<Card>> grouped = Rules.groupBy(cards, Card::getValue, reverseOrder());
        return Rules.filterBySizeSortingByValues(grouped, 3).findFirst()
                .map(RuleResult::matching)
                .orElse(RuleResult.NO_MATCH);
    }
}
