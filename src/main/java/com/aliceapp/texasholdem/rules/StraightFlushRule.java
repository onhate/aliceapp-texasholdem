package com.aliceapp.texasholdem.rules;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class StraightFlushRule implements Rule {

    private static final int SIZE = 5;

    @Override
    public int priority() {
        return 8;
    }

    @Override
    public String name() {
        return "Straight Flush";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < SIZE) {
            return RuleResult.NO_MATCH;
        }

        Map<Suites, List<Card>> grouped = Rules.groupBy(cards, Card::getSuite);
        return Rules.filterBySize(grouped, SIZE)
                .map(seq -> Rules.findMinimumSequence(seq, Card::getValue, SIZE))
                .filter(Objects::nonNull)
                .findFirst().map(RuleResult::matching)
                .orElse(RuleResult.NO_MATCH);
    }
}
