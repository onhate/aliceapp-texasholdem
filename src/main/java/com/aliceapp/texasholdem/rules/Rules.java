package com.aliceapp.texasholdem.rules;

import static java.util.Comparator.comparingInt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class Rules {

    private static final int ACE = 1;
    private static final int MAX_VALUE = 13;

    static <O, G> Map<G, List<O>> groupBy(List<O> items, Function<O, G> function) {
        return groupBy(items, function, null);
    }

    static <O, G> Map<G, List<O>> groupBy(List<O> items, Function<O, G> function, Comparator<G> comparator) {
        TreeMap<G, List<O>> grouped = new TreeMap<>(comparator);
        for (O item : items) {
            G value = function.apply(item);
            grouped.computeIfAbsent(value, init -> new ArrayList<>()).add(item);
        }
        return grouped;
    }

    static <O, G> Stream<List<O>> filterBySize(Map<G, List<O>> grouped, int size) {
        return grouped.values().stream()
                .filter(group -> group.size() >= size);
    }

    static <O extends Comparable<O>, G> Stream<List<O>> filterBySizeSortingByValues(Map<G, List<O>> grouped, int size) {
        return filterBySize(grouped, size)
                .sorted(Rules::compare)
                .collect(Collectors.toList())
                .stream();
    }

    static <O> Set<O> findMinimumSequence(List<O> items, ToIntFunction<O> retriever, int size) {
        if (items.size() < size) {
            return null;
        }

        // highest first
        items.sort(comparingInt(retriever).reversed());

        O ace = items.get(items.size() - 1);
        boolean hasAce = retriever.applyAsInt(ace) == ACE;

        O item1 = items.get(0);
        Set<O> result = new HashSet<>(size);

        // first will be highest, so if has MAX and ACE both make a sequence
        if (hasAce && retriever.applyAsInt(item1) == MAX_VALUE) {
            result.add(item1);
            result.add(ace);
        }

        for (int i = 1; i < items.size(); i++) {
            O item2 = items.get(i);

            int value1 = retriever.applyAsInt(item1);
            int value2 = retriever.applyAsInt(item2);
            if (value1 == value2) {
                continue;
            }

            if (value1 - 1 == value2) { // keys are sorted from higher to lower so value1 > value2
                result.add(item1);
                result.add(item2);

                if (result.size() == size) {
                    return result;
                }
            } else {
                result = new HashSet<>(size);
            }
            item1 = item2;
        }

        return null;
    }

    public static <O extends Comparable<O>> int compare(Collection<O> items1, Collection<O> items2) {
        if (items2 == null || items2.isEmpty()) {
            return 1;
        }
        if (items1 == null || items1.isEmpty()) {
            return -1;
        }

        Iterator<O> it1 = items1.iterator();
        Iterator<O> it2 = items2.iterator();
        while (it1.hasNext() && it2.hasNext()) {
            int result = it2.next().compareTo(it1.next());
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }
}
