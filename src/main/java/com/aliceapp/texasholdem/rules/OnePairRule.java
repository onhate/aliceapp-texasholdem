package com.aliceapp.texasholdem.rules;

import static java.util.Comparator.reverseOrder;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;
import java.util.Map;

public class OnePairRule implements Rule {

    @Override
    public int priority() {
        return 1;
    }

    @Override
    public String name() {
        return "One Pair";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < 2) {
            return RuleResult.NO_MATCH;
        }

        Map<Integer, List<Card>> grouped = Rules.groupBy(cards, Card::getValue, reverseOrder()); // higher first
        return Rules.filterBySize(grouped, 2).findFirst()
                .map(RuleResult::matching)
                .orElse(RuleResult.NO_MATCH);
    }
}
