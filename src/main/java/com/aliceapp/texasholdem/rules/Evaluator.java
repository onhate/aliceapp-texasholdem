package com.aliceapp.texasholdem.rules;

import com.aliceapp.texasholdem.model.Card;
import java.util.Comparator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Evaluator {

    private Set<Rule> rules;

    public Evaluator() {
        // higher priority first
        rules = new TreeSet<>(Comparator.comparingInt(Rule::priority).reversed());
        ServiceLoader.load(Rule.class).iterator()
                .forEachRemaining(rule -> rules.add(rule));
    }

    public List<RuleResult> evaluate(List<Card> cards) {
        return rules.stream()
                .map(rule -> {
                    RuleResult result = rule.evaluate(cards);
                    result.setName(rule.name());
                    result.setPriority(rule.priority());
                    return result;
                })
                .filter(RuleResult::matches)
                .collect(Collectors.toList());
    }
}
