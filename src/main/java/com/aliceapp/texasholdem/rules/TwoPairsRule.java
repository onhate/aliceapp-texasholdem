package com.aliceapp.texasholdem.rules;

import static java.util.Comparator.reverseOrder;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections4.ListUtils;

public class TwoPairsRule implements Rule {

    @Override
    public int priority() {
        return 2;
    }

    @Override
    public String name() {
        return "Two Pairs";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < 4) {
            return RuleResult.NO_MATCH;
        }

        Map<Integer, List<Card>> grouped = Rules.groupBy(cards, Card::getValue, reverseOrder()); // higher first
        List<List<Card>> pairs = Rules.filterBySize(grouped, 2).collect(Collectors.toList());

        // we need at least 2 pairs
        if (pairs.size() >= 2) {
            List<Card> first = pairs.get(0);
            List<Card> second = pairs.get(1);
            return RuleResult.matching(ListUtils.union(first, second));
        }

        return RuleResult.NO_MATCH;
    }
}
