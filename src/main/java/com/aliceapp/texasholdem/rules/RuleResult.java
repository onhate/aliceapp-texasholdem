package com.aliceapp.texasholdem.rules;

import static org.apache.commons.lang3.StringUtils.join;

import com.aliceapp.texasholdem.model.Card;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;
import lombok.Data;

@Data
public class RuleResult implements Comparable<RuleResult> {

    static final RuleResult NO_MATCH = new RuleResult();

    static RuleResult matching(Collection<Card> cards) {
        RuleResult result = new RuleResult();
        result.setMatch(true);
        result.setCards(cards);
        return result;
    }

    private boolean match;
    private String name;
    private int priority;
    private Collection<Card> cards;

    boolean matches() {
        return match;
    }

    public void setCards(Collection<Card> cards) {
        this.cards = new TreeSet<>(Comparator.reverseOrder());
        this.cards.addAll(cards);
    }

    @Override
    public String toString() {
        return String.format("%s (%d) -> %s", name, priority, join(cards, " "));
    }

    @Override
    public int compareTo(RuleResult other) {
        if (priority < other.priority) {
            return 1;
        }
        if (priority > other.priority) {
            return -1;
        }
        return Rules.compare(cards, other.cards);
    }
}
