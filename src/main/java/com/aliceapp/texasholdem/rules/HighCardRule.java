package com.aliceapp.texasholdem.rules;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;

public class HighCardRule implements Rule {

    @Override
    public int priority() {
        return 0;
    }

    @Override
    public String name() {
        return "High Card";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.isEmpty()) {
            return RuleResult.NO_MATCH;
        }
        return RuleResult.matching(cards); // send all cards for later comparision
    }
}
