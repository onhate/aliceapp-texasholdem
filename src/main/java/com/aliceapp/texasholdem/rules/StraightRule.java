package com.aliceapp.texasholdem.rules;

import static java.util.Objects.nonNull;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;
import java.util.Set;

public class StraightRule implements Rule {

    private static final int SIZE = 5;

    @Override
    public int priority() {
        return 4;
    }

    @Override
    public String name() {
        return "Straight";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < SIZE) {
            return RuleResult.NO_MATCH;
        }

        Set<Card> sequence = Rules.findMinimumSequence(cards, Card::getValue, SIZE);
        return nonNull(sequence) ? RuleResult.matching(sequence) : RuleResult.NO_MATCH;
    }
}
