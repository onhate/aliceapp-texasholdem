package com.aliceapp.texasholdem.rules;

import static java.util.Comparator.reverseOrder;

import com.aliceapp.texasholdem.model.Card;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class FourOfAKindRule implements Rule {

    @Override
    public int priority() {
        return 7;
    }

    @Override
    public String name() {
        return "Four of a Kind";
    }

    @Override
    public RuleResult evaluate(List<Card> cards) {
        if (cards.size() < 4) {
            return RuleResult.NO_MATCH;
        }

        Map<Integer, List<Card>> grouped = Rules.groupBy(cards, Card::getValue, reverseOrder());
        return Rules.filterBySizeSortingByValues(grouped, 4).findFirst()
                .map(RuleResult::matching)
                .orElse(RuleResult.NO_MATCH);
    }
}
