package com.aliceapp.texasholdem.rules;

import com.aliceapp.texasholdem.model.Card;
import java.util.List;

interface Rule {

    int priority();

    String name();

    RuleResult evaluate(List<Card> cards);
}
