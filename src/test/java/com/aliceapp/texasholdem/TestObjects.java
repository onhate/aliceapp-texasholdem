package com.aliceapp.texasholdem;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;

public final class TestObjects {

    public static Card card(int value, Suites suite) {
        return new Card(value, suite);
    }
}
