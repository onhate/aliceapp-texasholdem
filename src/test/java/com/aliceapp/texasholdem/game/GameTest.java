package com.aliceapp.texasholdem.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.PlayAction;
import com.aliceapp.texasholdem.model.Player;
import com.aliceapp.texasholdem.utils.CircularList;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import org.mockito.Answers;

public class GameTest {

    @Test
    public void shouldNotInitializePlayersWhenAlreadySetOnConstructor() {
        // arrange
        CircularList<Player> players = players();
        Game game = new Game(players);

        // act
        game.distributeCards();

        // assert
        assertEquals(game.getPlayer(0), players.get(0));
        assertEquals(game.getPlayer(1), players.get(1));
        assertEquals(game.getPlayer(2), players.get(2));
        assertEquals(game.getPlayer(3), players.get(3));
    }

    @Test
    public void shouldInitializeAllPlayersWith3CardsAndDeckWith4() {
        // arrange
        CircularList<Player> players = players();
        Game game = new Game(players);

        // act
        game.distributeCards();

        // assert
        assertEquals(game.getPlayer(0).getCards().size(), 2);
        assertEquals(game.getPlayer(1).getCards().size(), 2);
        assertEquals(game.getBoard().getCards().size(), 5);
    }

    @Test
    public void shouldInitializePlayersAndDeckWithDistinctCards() {
        // arrange
        Game game = new Game(players());

        // act
        game.distributeCards();

        // assert
        Set<Card> cards = new HashSet<>();
        cards.addAll(game.getPlayer(0).getCards());
        cards.addAll(game.getPlayer(1).getCards());
        cards.addAll(game.getPlayer(2).getCards());
        cards.addAll(game.getBoard().getCards());
        assertEquals(cards.size(), (3 * 2) + 5);
    }

    @Test
    public void shouldCallPlayForAllPlayersOnIterate() {
        // arrange
        CircularList<Player> players = players();
        Game game = new Game(players);
        game.distributeCards();

        // act
        game.iterate();
        game.iterate();
        game.iterate();
        game.iterate();

        // assert
        verify(players.get(0), times(4)).play(eq(game), any());
        verify(players.get(1), times(4)).play(eq(game), any());
        verify(players.get(2), times(4)).play(eq(game), any());
    }

    @Test
    public void shouldRevealAllCardsFromDeckAfterIteration() {
        // arrange
        Game game = new Game(players());
        game.distributeCards();

        // act
        game.iterate();
        game.iterate();
        game.iterate();
        game.iterate();

        // assert
        assertTrue(game.getBoardCards().isEmpty());
        assertEquals(game.getBoardRevealed().size(), 5);
    }

    private CircularList<Player> players() {
        Player player1 = mock(Player.class, Answers.CALLS_REAL_METHODS);
        when(player1.play(any(), any())).thenReturn(PlayAction.CHECK);

        Player player2 = mock(Player.class, Answers.CALLS_REAL_METHODS);
        when(player2.play(any(), any())).thenReturn(PlayAction.CHECK);

        Player player3 = mock(Player.class, Answers.CALLS_REAL_METHODS);
        when(player3.play(any(), any())).thenReturn(PlayAction.CHECK);

        CircularList<Player> players = new CircularList<>();
        players.add(player1);
        players.add(player2);
        players.add(player3);
        return players;
    }
}