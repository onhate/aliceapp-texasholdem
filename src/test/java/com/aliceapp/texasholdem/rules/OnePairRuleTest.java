package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.Arrays;
import org.junit.Test;

public class OnePairRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new OnePairRule().priority()).isEqualTo(1);
    }

    @Test
    public void shouldReturnNoMatchWhenNoPairs() {
        // arrange
        Rule rule = new OnePairRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(Arrays.asList(card1, card2, card3));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnPairOf1s() {
        // arrange
        Rule rule = new OnePairRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);
        Card card3 = card(3, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(Arrays.asList(card1, card2, card3));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsExactly(card1, card2);
    }


    @Test
    public void shouldReturnPairOf2sWhenComparingWithPairsOf1s() {
        // arrange
        Rule rule = new OnePairRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);
        Card card3 = card(2, Suites.HEARTS);
        Card card4 = card(2, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(Arrays.asList(card1, card2, card3, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card3, card4);
    }
}