package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import org.junit.Test;

public class RoyalFlushRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new RoyalFlushRule().priority()).isEqualTo(9);
    }

    @Test
    public void shouldReturnNoMatchWhenDoesNotContainsMinimumOf5Cards() {
        // arrange
        Rule rule = new RoyalFlushRule();

        // act
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(11, Suites.HEARTS);
        Card card3 = card(12, Suites.HEARTS);
        Card card4 = card(13, Suites.HEARTS);
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldNotMatchWhen5OfSameKindInSequenceButNotRoyal() {
        // arrange
        Rule rule = new RoyalFlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(5, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldNotMatchWhenHighSequenceButNotSameSuite() {
        // arrange
        Rule rule = new RoyalFlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(10, Suites.HEARTS);
        Card card3 = card(11, Suites.HEARTS);
        Card card4 = card(12, Suites.HEARTS);
        Card card5 = card(13, Suites.SPADES);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnFlushWhenMatches() {
        // arrange
        Rule rule = new RoyalFlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(10, Suites.HEARTS);
        Card card3 = card(11, Suites.HEARTS);
        Card card4 = card(12, Suites.HEARTS);
        Card card5 = card(13, Suites.HEARTS);
        Card card6 = card(6, Suites.CLUBS);
        Card card7 = card(7, Suites.SPADES);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4, card6, card7));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }
}