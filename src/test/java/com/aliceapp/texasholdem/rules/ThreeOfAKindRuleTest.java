package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import org.junit.Test;

public class ThreeOfAKindRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new ThreeOfAKindRule().priority()).isEqualTo(3);
    }

    @Test
    public void shouldReturnNoMatchWhenNo3OfAKind() {
        // arrange
        Rule rule = new ThreeOfAKindRule();
        Card card1 = card(7, Suites.HEARTS);
        Card card2 = card(7, Suites.DIAMONDS);
        Card card3 = card(6, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnMatchWhen3OfAKind() {
        // arrange
        Rule rule = new ThreeOfAKindRule();
        Card card1 = card(7, Suites.HEARTS);
        Card card2 = card(7, Suites.DIAMONDS);
        Card card3 = card(7, Suites.SPADES);
        Card card4 = card(3, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3);
    }

    @Test
    public void shouldReturnHighest3OfAKindWhenMultiple() {
        // arrange
        Rule rule = new ThreeOfAKindRule();
        Card card1 = card(3, Suites.HEARTS);
        Card card2 = card(3, Suites.SPADES);
        Card card3 = card(3, Suites.DIAMONDS);
        Card card4 = card(6, Suites.DIAMONDS);
        Card card5 = card(6, Suites.CLUBS);
        Card card6 = card(6, Suites.SPADES);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card4, card5, card6);
    }
}