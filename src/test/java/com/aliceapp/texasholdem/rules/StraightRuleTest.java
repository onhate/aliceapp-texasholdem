package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.List;
import org.junit.Test;

public class StraightRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new StraightRule().priority()).isEqualTo(4);
    }

    @Test
    public void shouldReturnNoMatchWhenDoesNotContainsMinimumOf5Cards() {
        // arrange
        Rule rule = new StraightRule();

        // act
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.CLUBS);
        Card card4 = card(4, Suites.CLUBS);
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnSequenceContains5InSequence() {
        // arrange
        Rule rule = new StraightRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.CLUBS);
        Card card4 = card(4, Suites.CLUBS);
        Card card5 = card(5, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }

    @Test
    public void shouldReturnSequenceContains5InSequenceWithAce() {
        // arrange
        Rule rule = new StraightRule();
        Card card1 = card(10, Suites.HEARTS);
        Card card2 = card(11, Suites.HEARTS);
        Card card3 = card(12, Suites.CLUBS);
        Card card4 = card(13, Suites.CLUBS);
        Card card5 = card(1, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }

    @Test
    public void shouldReturnSequenceContains5InSequenceRepeatingOne() {
        // arrange
        Rule rule = new StraightRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.DIAMONDS);
        Card card4 = card(3, Suites.CLUBS);
        Card card5 = card(4, Suites.CLUBS);
        Card card6 = card(5, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card6, card3, card1, card5, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card5, card6);
    }

    @Test
    public void shouldReturnHighestSequenceWhenMultiple() {
        // arrange
        Rule rule = new StraightRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.CLUBS);
        Card card4 = card(4, Suites.CLUBS);
        Card card5 = card(5, Suites.CLUBS);
        Card card6 = card(6, Suites.DIAMONDS);
        Card card7 = card(7, Suites.SPADES);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4, card7, card6));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card3, card4, card5, card6, card7);
    }

    @Test
    public void shouldReturnHighestSequenceWhenMultipleDistinct() {
        // arrange
        Rule rule = new StraightRule();
        Card card1 = card(2, Suites.HEARTS);
        Card card2 = card(3, Suites.HEARTS);
        Card card3 = card(4, Suites.CLUBS);
        Card card4 = card(5, Suites.CLUBS);
        Card card5 = card(6, Suites.CLUBS);

        Card card9 = card(9, Suites.SPADES);
        Card card10 = card(10, Suites.SPADES);
        Card card11 = card(11, Suites.SPADES);
        Card card12 = card(12, Suites.SPADES);
        Card card13 = card(13, Suites.SPADES);

        List<Card> cards = asList(card2, card5, card3, card1, card4, card9, card10, card11, card12, card13);

        // act
        RuleResult result = rule.evaluate(cards);

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card9, card10, card11, card12, card13);
    }

    @Test
    public void shouldReturnSequenceWhenContainingIncompleteHigherSequence() {
        // arrange
        Rule rule = new StraightRule();
        Card card1 = card(2, Suites.HEARTS);
        Card card2 = card(3, Suites.HEARTS);
        Card card3 = card(4, Suites.CLUBS);
        Card card4 = card(5, Suites.CLUBS);
        Card card5 = card(6, Suites.CLUBS);

        Card card10 = card(10, Suites.SPADES);
        Card card11 = card(11, Suites.SPADES);
        Card card12 = card(12, Suites.SPADES);
        Card card13 = card(13, Suites.SPADES);

        List<Card> cards = asList(card2, card5, card3, card1, card4, card10, card11, card12, card13);

        // act
        RuleResult result = rule.evaluate(cards);

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }
}