package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.List;
import org.junit.Test;

public class FlushRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new FlushRule().priority()).isEqualTo(5);
    }

    @Test
    public void shouldReturnNoMatchWhenNotFlush() {
        // arrange
        Rule rule = new FlushRule();
        Card card1 = card(1, Suites.CLUBS);
        Card card2 = card(2, Suites.CLUBS);
        Card card3 = card(3, Suites.CLUBS);
        Card card4 = card(5, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnMatchWhenFlush() {
        // arrange
        Rule rule = new FlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(5, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }

    @Test
    public void shouldReturnHighestFlushWhenMultiple() {
        // arrange
        Rule rule = new FlushRule();

        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(4, Suites.HEARTS);
        Card card3 = card(6, Suites.HEARTS);
        Card card4 = card(7, Suites.HEARTS);
        Card card5 = card(10, Suites.HEARTS);

        Card card6 = card(6, Suites.CLUBS);
        Card card7 = card(7, Suites.CLUBS);
        Card card8 = card(9, Suites.CLUBS);
        Card card9 = card(10, Suites.CLUBS);
        Card card10 = card(11, Suites.CLUBS);
        List<Card> cards = asList(card1, card2, card3, card4, card5, card6, card7, card8, card9, card10);

        // act
        RuleResult result = rule.evaluate(cards);

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card6, card7, card8, card9, card10);
    }
}