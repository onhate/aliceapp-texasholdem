package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import org.junit.Test;

public class StraightFlushRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new StraightFlushRule().priority()).isEqualTo(8);
    }

    @Test
    public void shouldReturnNoMatchWhenDoesNotContainsMinimumOf5Cards() {
        // arrange
        Rule rule = new StraightFlushRule();

        // act
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.CLUBS);
        Card card4 = card(4, Suites.CLUBS);
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnSequenceContains5OfSameKindInSequence() {
        // arrange
        Rule rule = new StraightFlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(5, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }

    @Test
    public void shouldReturnHighestFlushWhenMultiple() {
        // arrange
        Rule rule = new StraightFlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(5, Suites.HEARTS);
        Card card6 = card(6, Suites.HEARTS);
        Card card7 = card(7, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4, card6, card7));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card3, card4, card5, card6, card7);
    }

    @Test
    public void shouldReturnHighestFlushWhenMultipleAndAce() {
        // arrange
        Rule rule = new StraightFlushRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(5, Suites.HEARTS);

        Card card6 = card(10, Suites.HEARTS);
        Card card7 = card(11, Suites.HEARTS);
        Card card8 = card(12, Suites.HEARTS);
        Card card9 = card(13, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(asList(card2, card5, card3, card1, card4, card6, card7, card8, card9));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card6, card7, card8, card9);
    }

}