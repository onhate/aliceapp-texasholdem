package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import org.junit.Test;

public class HighCardRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new HighCardRule().priority()).isEqualTo(0);
    }

    @Test
    public void shouldReturnSingleCardWhenSingleCard() {
        // arrange
        Rule rule = new HighCardRule();
        Card card = card(1, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(singletonList(card));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card);
    }

    @Test
    public void shouldReturnHighestCardWhenTwoCards() {
        // arrange
        Rule rule = new HighCardRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.DIAMONDS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2);
    }

    @Test
    public void shouldReturnHighestCardBySuiteWhenConflicts() {
        // arrange
        Rule rule = new HighCardRule();
        Card card1 = card(11, Suites.HEARTS);
        Card card2 = card(11, Suites.DIAMONDS);
        Card card3 = card(11, Suites.CLUBS);
        Card card4 = card(11, Suites.SPADES);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4);
    }
}