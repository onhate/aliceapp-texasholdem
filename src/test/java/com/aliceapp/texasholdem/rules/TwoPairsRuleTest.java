package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.Arrays;
import org.junit.Test;

public class TwoPairsRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new TwoPairsRule().priority()).isEqualTo(2);
    }

    @Test
    public void shouldReturnNoMatchWhenNo2Pairs() {
        // arrange
        Rule rule = new TwoPairsRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(Arrays.asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnMatchWhen2PairsFound() {
        // arrange
        Rule rule = new TwoPairsRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);
        Card card3 = card(11, Suites.HEARTS);
        Card card4 = card(11, Suites.CLUBS);
        Card card5 = card(2, Suites.DIAMONDS);
        Card card6 = card(13, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(Arrays.asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4);
    }

    @Test
    public void shouldReturnHighestPairs() {
        // arrange
        Rule rule = new TwoPairsRule();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);
        Card card3 = card(11, Suites.HEARTS);
        Card card4 = card(11, Suites.CLUBS);
        Card card5 = card(12, Suites.DIAMONDS);
        Card card6 = card(12, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(Arrays.asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card3, card4, card5, card6);
    }
}