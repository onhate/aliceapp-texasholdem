package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import java.util.Collections;
import java.util.List;
import org.junit.Test;

public class EvaluatorTest {

    @Test
    public void shouldReturnEmptyWhenNoCards() {
        // arrange
        Evaluator evaluator = new Evaluator();

        // act
        List<RuleResult> result = evaluator.evaluate(Collections.emptyList());

        // assert
        assertThat(result).isEmpty();
    }

    @Test
    public void shouldReturnHighCardAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);

        // act
        List<RuleResult> result = evaluator.evaluate(Collections.singletonList(card1));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("High Card");
        assertThat(result.get(0).getCards()).containsOnly(card1);
    }

    @Test
    public void shouldReturnOnePairAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();

        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("One Pair");
        assertThat(result.get(0).getCards()).containsOnly(card1, card2);
    }

    @Test
    public void shouldReturnTwoPairsAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);
        Card card3 = card(2, Suites.HEARTS);
        Card card4 = card(2, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Two Pairs");
        assertThat(result.get(0).getCards()).containsOnly(card1, card2, card3, card4);
    }

    @Test
    public void shouldReturnThreeOfAKindAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.SPADES);
        Card card3 = card(1, Suites.DIAMONDS);
        Card card4 = card(2, Suites.SPADES);
        Card card5 = card(3, Suites.CLUBS);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Three of a Kind");
        assertThat(result.get(0).getCards()).containsOnly(card1, card2, card3);
    }

    @Test
    public void shouldReturnStraightAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.SPADES);
        Card card3 = card(3, Suites.DIAMONDS);
        Card card4 = card(4, Suites.SPADES);
        Card card5 = card(5, Suites.CLUBS);
        Card card6 = card(1, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Straight");
        assertThat(result.get(0).getCards()).containsExactly(card5, card4, card3, card2, card1);
    }

    @Test
    public void shouldReturnFlushAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(6, Suites.HEARTS);
        Card card6 = card(1, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Flush");
        assertThat(result.get(0).getCards()).containsExactly(card5, card4, card3, card2, card1);
    }

    @Test
    public void shouldReturnFullHouseAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(1, Suites.CLUBS);
        Card card3 = card(1, Suites.DIAMONDS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(4, Suites.DIAMONDS);
        Card card6 = card(5, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Full House");
        assertThat(result.get(0).getCards()).containsOnly(card5, card4, card3, card2, card1);
    }

    @Test
    public void shouldReturnFourOfAKindAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.SPADES);
        Card card2 = card(1, Suites.CLUBS);
        Card card3 = card(1, Suites.HEARTS);
        Card card4 = card(1, Suites.DIAMONDS);
        Card card5 = card(4, Suites.DIAMONDS);
        Card card6 = card(4, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Four of a Kind");
        assertThat(result.get(0).getCards()).contains(card4, card3, card2, card1);
    }

    @Test
    public void shouldReturnStraightFlushAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(1, Suites.HEARTS);
        Card card2 = card(2, Suites.HEARTS);
        Card card3 = card(3, Suites.HEARTS);
        Card card4 = card(4, Suites.HEARTS);
        Card card5 = card(5, Suites.HEARTS);
        Card card6 = card(1, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Straight Flush");
        assertThat(result.get(0).getCards()).containsExactly(card5, card4, card3, card2, card1);
    }

    @Test
    public void shouldReturnRoyalFlushAsFirstResult() {
        // arrange
        Evaluator evaluator = new Evaluator();
        Card card1 = card(10, Suites.HEARTS);
        Card card2 = card(11, Suites.HEARTS);
        Card card3 = card(12, Suites.HEARTS);
        Card card4 = card(13, Suites.HEARTS);
        Card card5 = card(1, Suites.HEARTS);
        Card card6 = card(1, Suites.SPADES);

        // act
        List<RuleResult> result = evaluator.evaluate(asList(card1, card2, card3, card4, card5, card6));

        // assert
        assertThat(result).isNotEmpty();
        assertThat(result.get(0).getName()).isEqualTo("Royal Flush");
        assertThat(result.get(0).getCards()).containsExactly(card4, card3, card2, card1, card5);
    }

}