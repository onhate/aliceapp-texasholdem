package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import org.junit.Test;

public class FourOfAKindRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new FourOfAKindRule().priority()).isEqualTo(7);
    }

    @Test
    public void shouldReturnNoMatchWhenNotHavingMinimumOfCards() {
        // arrange
        Rule rule = new FourOfAKindRule();
        Card card1 = card(3, Suites.HEARTS);
        Card card2 = card(3, Suites.SPADES);
        Card card3 = card(3, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnNoMatchWhenNo4OfAKind() {
        // arrange
        Rule rule = new FourOfAKindRule();
        Card card1 = card(6, Suites.HEARTS);
        Card card2 = card(6, Suites.SPADES);
        Card card3 = card(6, Suites.CLUBS);
        Card card4 = card(7, Suites.HEARTS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnMatchWhen4fAKind() {
        // arrange
        Rule rule = new FourOfAKindRule();
        Card card1 = card(5, Suites.HEARTS);
        Card card2 = card(5, Suites.SPADES);
        Card card3 = card(5, Suites.CLUBS);
        Card card4 = card(5, Suites.DIAMONDS);
        Card card5 = card(4, Suites.SPADES);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4);
    }

    @Test
    public void shouldReturnHighest4OfAKindWhenMultiple() {
        // arrange
        Rule rule = new FourOfAKindRule();
        Card card1 = card(3, Suites.HEARTS);
        Card card2 = card(3, Suites.SPADES);
        Card card3 = card(3, Suites.CLUBS);
        Card card4 = card(3, Suites.DIAMONDS);
        Card card5 = card(5, Suites.HEARTS);
        Card card6 = card(5, Suites.SPADES);
        Card card7 = card(5, Suites.CLUBS);
        Card card8 = card(5, Suites.DIAMONDS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5, card6, card7, card8));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card5, card6, card7, card8);
    }

}