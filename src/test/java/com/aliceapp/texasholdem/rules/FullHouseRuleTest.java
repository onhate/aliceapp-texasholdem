package com.aliceapp.texasholdem.rules;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.aliceapp.texasholdem.model.Card;
import com.aliceapp.texasholdem.model.Suites;
import org.junit.Test;

public class FullHouseRuleTest {

    @Test
    public void assertPriority() {
        assertThat(new FullHouseRule().priority()).isEqualTo(6);
    }

    @Test
    public void shouldReturnNoMatchWhenNotHavingMinimumOf5Cards() {
        // arrange
        Rule rule = new FullHouseRule();
        Card card1 = card(11, Suites.SPADES);
        Card card2 = card(12, Suites.HEARTS);
        Card card3 = card(13, Suites.CLUBS);
        Card card4 = card(4, Suites.DIAMONDS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnNoMatchWhenTripleButNotPair() {
        // arrange
        Rule rule = new FullHouseRule();
        Card card1 = card(11, Suites.SPADES);
        Card card2 = card(11, Suites.HEARTS);
        Card card3 = card(11, Suites.CLUBS);
        Card card4 = card(4, Suites.DIAMONDS);
        Card card5 = card(5, Suites.DIAMONDS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnNoMatchWhenPairButNotTriple() {
        // arrange
        Rule rule = new FullHouseRule();
        Card card1 = card(11, Suites.SPADES);
        Card card2 = card(11, Suites.HEARTS);
        Card card3 = card(4, Suites.CLUBS);
        Card card4 = card(4, Suites.DIAMONDS);
        Card card5 = card(5, Suites.DIAMONDS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5));

        // assert
        assertFalse(result.matches());
    }

    @Test
    public void shouldReturnMatchWhenFullHouse() {
        // arrange
        Rule rule = new FullHouseRule();
        Card card1 = card(5, Suites.HEARTS);
        Card card2 = card(5, Suites.SPADES);
        Card card3 = card(5, Suites.DIAMONDS);
        Card card4 = card(3, Suites.CLUBS);
        Card card5 = card(3, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card4, card5);
    }

    @Test
    public void shouldReturnHighestFullHouseWhenMultiple() {
        // arrange
        Rule rule = new FullHouseRule();
        Card card1 = card(5, Suites.HEARTS);
        Card card2 = card(5, Suites.SPADES);
        Card card3 = card(5, Suites.DIAMONDS);

        Card card4 = card(3, Suites.CLUBS);
        Card card5 = card(3, Suites.CLUBS);

        Card card6 = card(9, Suites.CLUBS);
        Card card7 = card(9, Suites.CLUBS);

        // act
        RuleResult result = rule.evaluate(asList(card1, card2, card3, card4, card5, card6, card7));

        // assert
        assertTrue(result.matches());
        assertThat(result.getCards()).containsOnly(card1, card2, card3, card6, card7);
    }

}