package com.aliceapp.texasholdem.model;

import static com.aliceapp.texasholdem.TestObjects.card;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.List;
import org.junit.Test;

public class BoardTest {

    @Test
    public void shouldInitializeWithACopyOfCards() {
        // arrange
        Board deck = new Board();
        List<Card> init = cards();

        // act
        deck.setCards(init);

        // assert
        assertNotSame(deck.getCards(), init);
    }

    @Test
    public void shouldHaveRevealed2CardsOnFirstPlay() {
        // arrange
        Board deck = new Board();
        deck.setCards(cards());

        // act
        deck.play();

        // assert
        assertEquals(deck.getCards().size(), 3);
        assertEquals(deck.getRevealed().size(), 2);
    }

    @Test
    public void shouldHaveRevealed3CardsOnSecondPlay() {
        // arrange
        Board deck = new Board();
        deck.setCards(cards());

        // act
        deck.play();
        deck.play();

        // assert
        assertEquals(deck.getCards().size(), 2);
        assertEquals(deck.getRevealed().size(), 3);
    }

    @Test
    public void shouldHaveRevealed4CardsOnThirdPlay() {
        // arrange
        Board deck = new Board();
        deck.setCards(cards());

        // act
        deck.play();
        deck.play();
        deck.play();

        // assert
        assertEquals(deck.getCards().size(), 1);
        assertEquals(deck.getRevealed().size(), 4);
    }

    @Test
    public void shouldNotFailOnFourthPlayJustSkip() {
        // arrange
        Board deck = new Board();
        deck.setCards(cards());

        // act
        deck.play();
        deck.play();
        deck.play();
        deck.play();
        // skipped
        deck.play();

        // assert
        assertEquals(deck.getCards().size(), 0);
        assertEquals(deck.getRevealed().size(), 5);
    }

    private List<Card> cards() {
        return asList(
                card(1, Suites.HEARTS),
                card(2, Suites.DIAMONDS),
                card(3, Suites.SPADES),
                card(4, Suites.CLUBS),
                card(5, Suites.CLUBS)
        );
    }
}