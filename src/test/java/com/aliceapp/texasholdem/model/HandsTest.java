package com.aliceapp.texasholdem.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HandsTest {

    @Test
    public void shouldInitializeWith012() {
        // arrange
        Hands hand = new Hands();

        // assert
        assertEquals(hand.getSmall(), 0);
        assertEquals(hand.getBig(), 1);
        assertEquals(hand.getDealer(), 2);
    }

    @Test
    public void shouldIncrementAllVariables() {
        // arrange
        Hands hand = new Hands();

        // act
        hand.increment();
        hand.increment();

        // assert
        assertEquals(hand.getSmall(), 2);
        assertEquals(hand.getBig(), 3);
        assertEquals(hand.getDealer(), 4);
    }
}