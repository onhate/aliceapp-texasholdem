package com.aliceapp.texasholdem.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CardTest {

    @Test
    public void shouldReturnCorrectSuiteOnString() {
        // arrange
        Card card1 = new Card(10, Suites.HEARTS);
        Card card2 = new Card(11, Suites.DIAMONDS);
        Card card3 = new Card(12, Suites.SPADES);
        Card card4 = new Card(13, Suites.CLUBS);

        // act
        String name1 = card1.toString();
        String name2 = card2.toString();
        String name3 = card3.toString();
        String name4 = card4.toString();

        // assert
        assertThat(name1).isEqualTo("\ud83c\udcba");
        assertThat(name2).isEqualTo("\ud83c\udccb");
        assertThat(name3).isEqualTo("\ud83c\udcad");
        assertThat(name4).isEqualTo("\ud83c\udcde");
    }
}