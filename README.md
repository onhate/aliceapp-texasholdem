# Texas Hold'em

## Building

`./gradlew clean build`

## Running

`java -jar build/libs/texasholdem-all-1.0-SNAPSHOT.jar`

### Running without building

Download the release jar from here [texasholdem-1.0-SNAPSHOT.jar](https://bitbucket.org/onhate/aliceapp-texasholdem/downloads/texasholdem-1.0-SNAPSHOT.jar)